import glob
import os
import re
import operator

# Handle all of our document loading and word counting needs
class WordProcessor:
    def __init__(self, inputFiles, outputFiles):
        self.inputFiles = inputFiles
        self.outputFiles = outputFiles
        self.stopwordsLookup = {}
        self.wordIndexEntries = {}
        self.sentenceRegexp = r"\”?((?:[A-Z]|\d)(?:.|\s)*?(?:[^A-Z]\S(?:\.|\?|\!)(?:\"|\”)?))(?:\s|$)"
        self.wordRegexp = r"(?:\b(?:\w|\'|\-)+\b)|(?:\s\b(?:\b[A-Z]\w+\W)+[A-Z]\w+\b)"
        self.documents = []

    def loadDocuments(self):
        pathspec  = self.inputFiles["documents"] 
        docnames = []
        documents = []
        for filename in glob.glob(pathspec):
            with open(os.path.join(os.getcwd(), filename), 'r') as f: 
                docnames.append(filename)
                fileContents = f.read()
                documents.append({
                    "filename": filename,
                    "contents": fileContents
                })
        self.documents = documents

    def loadStopwords(self):
        stopwords = []
        stopwordsFile = self.inputFiles["stopwords"] 
        with open(os.path.join(os.getcwd(), stopwordsFile), 'r') as f: 
            stopwords = f.readlines()

        return self.process_stopwords(stopwords)

    def setStopwords(self, stopwords):
        self.stopwordsLookup = stopwords

    # Extract array of sentences from entire document
    def extract_sentences(self, doc):
        pattern = self.sentenceRegexp
        p = re.compile(pattern)
        sentences = p.findall(doc)
        return sentences

    # Extract array or words from sentence
    def extract_words(self, sentence):
        pattern = self.wordRegexp
        p = re.compile(pattern)
        words = p.findall(sentence)
        return words

    # Read stopwords file, strip white space and lower case, convert to dictionary lookup.
    def process_stopwords(self, stopwords):
        stopwordsLookup = {}
        for w in stopwords:
            w = w.strip().lower()
            stopwordsLookup[w] = True

        return stopwordsLookup

    # For each word encountered, count and index, or discard if a stopword.
    def register_word( self, word, i, j):
        word = word.strip().lower()

        if self.stopwordsLookup.get(word) is not None:
            return

        index = (i,j)

        wordIndexEntry = self.wordIndexEntries.get(word)
        if wordIndexEntry is None:
            wordIndexEntry = {
                "word": word,
                "indices": [index],
                "count": 1
            }
            self.wordIndexEntries[word] = wordIndexEntry
        else:
            wordIndexEntry["indices"].append(index)
            wordIndexEntry["count"] += 1


    # Process each document, deconstruct to sentences, deconstruct to words, index the words.
    def processDocuments(self):
        documents = self.documents
        sentenceDocLookup = {}
        for i in range(len(documents)):
            doc = documents[i]["contents"]
            sentences = self.extract_sentences(doc)
            for j in range(len(sentences)):
                sentence = sentences[j]
                sentenceDocLookup[i,j] = sentence
                words = self.extract_words(sentence)
                for k in range(len(words)):
                    word = words[k]
                    self.register_word(word, i, j)
        
        mostCommonWords = sorted(list(self.wordIndexEntries.values()),key=operator.itemgetter('count'),reverse=True)
        
        return mostCommonWords, sentenceDocLookup

    # Write word list without count numbers, most common to least common.  Convenience feature.
    def outputStopwordCandidates(self, mostCommonWords):
        filename = self.outputFiles["stopwordcandidates"] 
        destpath = os.path.dirname(filename)
        if not os.path.exists(destpath): os.mkdir(destpath)
        with open(os.path.join(os.getcwd(), filename), 'w+') as f: 
            for item in mostCommonWords:
                word = item["word"]
                f.write(word + os.linesep)
        print(f"Stopword candidates written to {filename} ...")


    # Write word list *with* count numbers, most common to least common. 
    def outputWordsAndCounts(self, mostCommonWords):
        filename = self.outputFiles["word-count-list"] 
        destpath = os.path.dirname(filename)
        if not os.path.exists(destpath): os.mkdir(destpath)        
        with open(os.path.join(os.getcwd(), filename), 'w+') as f: 
            for item in mostCommonWords:
                f.write(f"{item['word']}\t\t{item['count']}" + os.linesep)
        print(f"Words and counts written to {filename} ...")

    # Write all words/counts, with containing documents/sentences.
    def outputWordsAndContainingSentences(self, mostCommonWords, sentenceDocLookup):
        filename = self.outputFiles["words-in-sentences"] 
        destpath = os.path.dirname(filename)
        if not os.path.exists(destpath): os.mkdir(destpath)        
        with open(os.path.join(os.getcwd(), filename), 'w+') as f: 
            for item in mostCommonWords:
                f.write(f"{item['word']}\t{item['count']}" + os.linesep)
                prev_index = ()
                prev_doc_id = -1
                for index in item["indices"]:
                    if index != prev_index:
                        doc_id = index[0]
                        if doc_id != prev_doc_id:
                            if prev_doc_id > -1: f.write(os.linesep)
                            f.write(f"{item['word']} usages in {self.documents[doc_id]['filename']}" + os.linesep)
                            prev_doc_id = doc_id

                        f.write(sentenceDocLookup[index] + os.linesep)
                    prev_index = index
                f.write(os.linesep + os.linesep)
        print(f"Words, counts and containing sentences written to {filename} ...")


    # Write all words/counts, with containing documents/sentences.  Readable layout in HTML.
    def htmlWordsAndContainingSentences(self, mostCommonWords, sentenceDocLookup):
        filename = self.outputFiles["html-words-in-sentences"] 
        with open(os.path.join(os.getcwd(), filename), 'w+') as f:
            f.write("""
<style>
    .header {width: 100%;}
    .sidebar {width: 20%; float:left;}
    .sentences {width: 80%; float:left; margin-bottom: 15px;}
    .sentence {clear: both; border-bottom: 1px solid gray}
</style>
""") 
            for item in mostCommonWords:
                f.write(f"<div class='wordheader'><b>{item['word']}</b>\t{item['count']}</div>" + os.linesep)
                prev_index = ()
                prev_doc_id = -1
                for index in item["indices"]:
                    if index != prev_index:
                        doc_id = index[0]
                        if doc_id != prev_doc_id:
                            if prev_doc_id > -1: 
                                f.write("</div>" + os.linesep)
                            f.write(f"<div class='sidebar'><b>{item['word']}</b> in {self.documents[doc_id]['filename']}</div>" + os.linesep)
                            f.write("<div class='sentences'>" + os.linesep)
                            prev_doc_id = doc_id

                        f.write("<div class='sentence'>" + sentenceDocLookup[index] + "</div>" + os.linesep)
                    
                    prev_index = index
                f.write("</div>" + os.linesep)
                f.write(os.linesep + os.linesep)
        print(f"HTML version of words, counts and containing sentences written to {filename} ...")



                





    
