from wordprocessor import WordProcessor

inputFiles = {
    "stopwords": "./stopwords.txt",
    "documents": "./import/*.txt"
}

outputFiles = {
    "stopwordcandidates": "./output/stopword-candidates.txt",
    "word-count-list": "./output/word-counts.txt",
    "words-in-sentences": "./output/word-in-sentences.txt",
    "html-words-in-sentences": "./output/word-in-sentences.html"
}

# Initialize class setting sources and destinations
wp = WordProcessor(inputFiles, outputFiles)

wp.loadDocuments()

# Load stopwords from file and set them in our class
stopwords = wp.loadStopwords()
wp.setStopwords(stopwords)

# Process our documents and derive word and sentence/doc indices.
mostCommonWords, sentenceDocLookup = wp.processDocuments()

# Write word list without counts for use in creating stoplist.
wp.outputStopwordCandidates(mostCommonWords)

# Write list of words and counts, most common to least common
wp.outputWordsAndCounts(mostCommonWords)

# Write list of words and docs/sentences containing them.
wp.outputWordsAndContainingSentences(mostCommonWords, sentenceDocLookup)

# Like previous, but formatted layout in HTML.
wp.htmlWordsAndContainingSentences(mostCommonWords, sentenceDocLookup)


