# Coding Assignment: Word extractions and counts from supplied documents.
Download this project by cloning from GitLab:

`git clone https://gitlab.com/w1111e/eigenwords.git`

This will create an `eigenwords` directory, where all of the following should run.  The sample documents will be copied into the `import` subdirectory.

If Python 3 is the default, then this can be run as follows:

`python eigenwords.py`

Otherwise run:

`python3 eigenwords.py`

There is a file `stopwords.txt` which is pre-populated with words to exclude.  Please edit as you see fit.  Or use the generated file `stopword-candidates.txt` to add to this list.

The program output should appear similar to this:
```
Stopword candidates written to ./output/stopword-candidates.txt ...
Words and counts written to ./output/word-counts.txt ...
Words, counts and containing sentences written to ./output/word-in-sentences.txt ...
HTML version of words, counts and containing sentences written to ./output/word-in-sentences.html ...
```

The following files should be found in a newly created `output` directory:

```
stopword-candidates.txt 
word-counts.txt         
word-in-sentences.html  
word-in-sentences.txt
```

`word-in-sentences.html` should be viewed in a browser.  This was tested in Chrome.

*Regular expressions can use some tweaking, but this is mostly there.  I took the liberty of defining runs of two or more capitalized words as proper names, and combining the words when recording them.*
